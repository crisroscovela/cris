<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>PublicationCreationView</title>
<%@ include file="Header.jsp"%>
</head>
<body>
<c:if test="${ri.id == user.id}">
<form action="CreatePublicationServlet" method="post">
        <input type="text" name="id" placeholder="Publication Id">
        <input type="text" name="title" placeholder="Title">
        <input type="text" name="publicationName" placeholder="Publication Name">
        <input type="text" name="publicationDate" placeholder="Publication Date">
        <input type="text" name="authors" placeholder="Authors">
        <button type="submit">Create publication</button>
</form>
</c:if>
</body>
</html>