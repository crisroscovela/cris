package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import es.upm.dit.apsv.cris.dao.ResearcherDAOImplementation;
import es.upm.dit.apsv.cris.model.Researcher;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final String ADMIN = "root";

	  @Override

	  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	    getServletContext().getRequestDispatcher("/LoginView.jsp").forward(req, resp);

	  }

	  @Override

	  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			String id = request.getParameter("id");
			String name = request.getParameter("name");
			String lastname = request.getParameter("lastname");
			String email = request.getParameter("email");
			
			
			Researcher researcher = new Researcher();
			researcher.setId(id);
			researcher.setName(name);
			researcher.setLastname(lastname);
			researcher.setEmail(email);
			
			
			ResearcherDAOImplementation.getInstance().create(researcher);
			List<Researcher> lr = new ArrayList<Researcher>();
			lr.add (researcher);
			request.getSession().setAttribute("researchers", lr);
			getServletContext().getRequestDispatcher("/AdminView.jsp").forward(request,response);
		  
	  } 

}
