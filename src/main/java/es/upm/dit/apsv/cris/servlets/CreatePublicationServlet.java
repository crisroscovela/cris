package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.model.Publication;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final String ADMIN = "root";

	  @Override

	  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	    getServletContext().getRequestDispatcher("/LoginView.jsp").forward(req, resp);

	  }

	  @Override

	  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			String id = request.getParameter("id");
			String title = request.getParameter("title");
			String publicationName = request.getParameter("publicationName");
			String publicationDate = request.getParameter("publicationDate");
			String authors = request.getParameter("authors");
			
			
			Publication publication = new Publication();
			publication.setId(id);
			publication.setTitle(title);
			publication.setPublicationName(publicationName);
			publication.setPublicationDate(publicationDate);
			publication.setAuthors(authors);
			
			PublicationDAOImplementation.getInstance().create(publication);
			List<Publication> lp = new ArrayList<Publication>();
			lp.add (publication);
			request.getSession().setAttribute("publications", lp);
			getServletContext().getRequestDispatcher("/PublicationCreationView.jsp").forward(request,response);
		  
	  } 

}
